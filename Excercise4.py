#!/usr/bin/env python3

user_dictionary = {}

for i in range(5):
    key_input = input("Input an unique key: ")

    while key_input in user_dictionary:
        key_input = input("The key has to be unique. Give an other key: ")

    value_input = input("Give an value for the key: ")
    user_dictionary[key_input] = value_input

print(user_dictionary)

item_dictionary = {"Flower": ["Rose", "Tulip", "Sunflower", "Bluebell", "Cherry blossom"],
                   "Animal": ["Dog", "Cat", "Tiger", "Mouse", "Rat"],
                   "Furniture": ["Dresser", "Wardrobe", "Desk", "Bed", "Table"],
                   "Name": ["Maciej", "Mark", "Cornelius", "Jacob", "Adam"],
                   "Dish": ["Pizza", "Cake", "Spaghetti", "Fries", "Meatloaf"]}

print(item_dictionary)
