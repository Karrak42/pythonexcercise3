#!/usr/bin/env python3

tuple_of_numbers = tuple(range(1, 101))

print(tuple_of_numbers)
print("=" * 30)

five_element_tuple = (1, 2, 3, 4, 5)
u, w, x, y, z = five_element_tuple

print("U = {}".format(u))
print("W = {}".format(w))
print("X = {}".format(x))
print("Y = {}".format(y))
print("Z = {}".format(z))
