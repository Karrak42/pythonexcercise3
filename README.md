# Excercise_3 README.md

## Excercise1.py

### Contains two loop witch do the following:

#### *While loop* 
##### is generating the a random number in range(1,100) while she is not equal 20

#### *For loop*
##### is counting down from 20 to 0

## Excersise2.py 

### Containst three lists witch do the following:

#### *list_of_numbers* contains three elements witch are lists that contain one element
#### *number_list* appends all number between 110 and 115 to the list 
#### *float_list* conatins 5 floats and prints them on screen

## Excersise3.py

### Constains two tupples witch contain:

#### *tuple_of_numbers* contains 100 elements (numbers from 1 to 100)
#### *five_element_tuple* contains 5 elements witch are assigned to the letters u, w, x, y, z

## Excersise4.py

### Contains two dictionaries witch contain:

#### *user_dictionary* contains 5 unique keys and normal values
#### *item_dictionary* contains 5 keys:
##### - Flower
##### - Animal
##### - Furniture
##### - Name
##### - Dishes

#### The values of the keys are lists with random names