#!/usr/bin/env python3

list_of_numbers = [[2], [3], [4]]

number_list = []

for i in range(100, 116):
    number_list.append(i)

print("Number list: ")
print(number_list)
print("=" * 30)

float_list = [2.3, 4.3, 15.55, 21.22, 33.33]

for i in float_list:
    print(i)