#!/usr/bin/env python3

import random

a = random.randint(1, 100)
print(a)
while a != 20:
    a = random.randint(1, 100)
    print(a)

print("=" * 30)

for num in range(20, -1, -1):
    print(num)